//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file /include/DetectorConstruction.hh
/// \brief Definition of the DetectorConstruction class
//
//
//
//
#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "globals.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4SubtractionSolid.hh"
#include "G4RunManager.hh"

#include "Materials.hh"
#include "DetectorMessenger.hh"


class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction();
    virtual ~DetectorConstruction();

  public:
    virtual G4VPhysicalVolume* Construct();

    void BuildWorld           ();
    void BuildPMT             ();
    void BuildDefaultLG       ();
    void PlaceGeometry        ();
    void SetWorldVolume       (G4ThreeVector arg);
    void SetRotation          (G4ThreeVector arg);
    void SetTranslation       (G4ThreeVector arg);
    void SetPMTTranslation    (G4ThreeVector arg);
    void SetPMTDiameter       (G4double arg);
    void UseCADModel          (G4String fileName);

    void SetSurfaceModel      (const G4OpticalSurfaceModel model);
    void SetSurfaceFinish     (const G4OpticalSurfaceFinish finish);
    void SetSurfaceType       (const G4SurfaceType type);
    void SetSurfaceSigmaAlpha (G4double v);
    void AddSurfaceMPV        (const char* c, G4MaterialPropertyVector* mpv);

    std::vector< G4String > GetSDnames(){return m_SDnames;}

  private:
    G4ThreeVector*          m_worldDim;
    G4ThreeVector*          m_LGpos;
    G4ThreeVector*          m_pmtPos;
    G4RotationMatrix*       m_rotation;
    G4double                m_pmtDia;
    G4double                m_PMTthickness;
    G4bool                  m_ConstructionHasBeenDone;
    G4bool                  m_UsingCADmodel;

    G4Box*                  m_solidWorld;
    G4LogicalVolume*        m_logicWorld;
    G4VPhysicalVolume*      m_physWorld;

    //The light guide will probably be built from boolean solids
    G4LogicalVolume*        m_logicLG;
    G4VPhysicalVolume*      m_physLG;

    G4Tubs*                           m_fiberCore;
    G4LogicalVolume*                  m_logicCore;
    std::vector< G4VPhysicalVolume* > m_physCore;

    G4Tubs*                           m_fiberCladding;
    G4LogicalVolume*                  m_logicCladding;
    std::vector< G4VPhysicalVolume* > m_physCladding;

    G4Tubs*                           m_fiberBuffer;
    G4LogicalVolume*                  m_logicBuffer;
    std::vector< G4VPhysicalVolume* > m_physBuffer;

    G4Tubs*                 m_solidPMT;
    G4LogicalVolume*        m_logicPMT;
    G4VPhysicalVolume*      m_physPMT;

    std::vector< G4Box* >             m_solidIntSD;
    std::vector< G4LogicalVolume* >   m_logicIntSD;
    std::vector< G4VPhysicalVolume* > m_physIntSD;

    G4LogicalBorderSurface* m_Surface;

    Materials*              materials;

    G4RunManager*           m_runMan;
    DetectorMessenger*      m_DetectorMessenger;

    std::vector< G4String > m_SDnames;
};

#endif /*DetectorConstruction_h*/
