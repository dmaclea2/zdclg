// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file /src/RunAction.cc
/// \brief Implementation of the RunAction class
//
// @Author Chad Lantz


// Make this appear first!
#include "G4Timer.hh"

#include "G4RunManager.hh"
#include "G4Run.hh"

#include "RunAction.hh"
#include "EventAction.hh"
#include "lgAnalysis.hh"
#include "G4SDManager.hh"
#include "DetectorConstruction.hh"
#include "PMTSD.hh"

#include "G4VAnalysisManager.hh"
#include "G4SystemOfUnits.hh"


/*
 *
 */
RunAction::RunAction(G4String fileName)
 : G4UserRunAction(),fTimer(0){
  fTimer = new G4Timer;
  m_fileName = fileName;

  G4RunManager::GetRunManager()->SetPrintProgress(1);

}

/*
 *
 */
RunAction::~RunAction(){
  delete fTimer;
  delete G4AnalysisManager::Instance();
}

/*
 *
 */
void RunAction::BeginOfRunAction(const G4Run* aRun){
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
  fTimer->Start();

  // Create analysis manager. The choice of analysis
  // technology is done via selection of a namespace
  // in B4Analysis.hh
  auto analysisManager = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManager->GetType() << G4endl;

  // Open an output file
  if(m_fileName == "") m_fileName = "output";
  analysisManager->OpenFile(m_fileName);


  // Create directories
  analysisManager->SetVerboseLevel(1);
  // analysisManager->SetNtupleMerging(true);

  DetectorConstruction* detectorConstruction = (DetectorConstruction*)G4RunManager::GetRunManager()->GetUserDetectorConstruction();
  std::vector< G4String > SDnames = detectorConstruction->GetSDnames();

  std::vector< std::vector<double>* > PtrVec;
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  PMTSD* theSD;
  // Create ntuples
  // Get the pointers held by each SD and assign them to a branch
  theSD = (PMTSD*)SDman->FindSensitiveDetector( "MyPMT" );
  theSD->SetNtupleNum(analysisManager->GetNofNtuples());
  PtrVec = theSD->GetVectors();
  analysisManager->CreateNtuple("lightGuide", "pos and momentum");
  analysisManager->CreateNtupleIColumn( 0, "EventNo" );
  analysisManager->CreateNtupleDColumn( 0, "x",       *PtrVec[0] );
  analysisManager->CreateNtupleDColumn( 0, "y",       *PtrVec[1] );
  analysisManager->CreateNtupleDColumn( 0, "z",       *PtrVec[2] );
  analysisManager->CreateNtupleDColumn( 0, "hitX",    *PtrVec[3] );
  analysisManager->CreateNtupleDColumn( 0, "hitY",    *PtrVec[4] );
  analysisManager->CreateNtupleDColumn( 0, "hitZ",    *PtrVec[5] );
  analysisManager->CreateNtupleDColumn( 0, "time",    *PtrVec[6] );
  analysisManager->CreateNtupleDColumn( 0, "theta",   *PtrVec[7] );
  analysisManager->CreateNtupleDColumn( 0, "energy",  *PtrVec[8] );
  analysisManager->FinishNtuple();

  G4int nTupleNo;
  for(uint i = 0; i < SDnames.size(); i++){
    if(SDnames[i] == "MyPMT"){
        continue; // We did this one first to make sure it is Ntuple 0
    }else{
      theSD = (PMTSD*)SDman->FindSensitiveDetector( SDnames[i] );
      PtrVec = theSD->GetVectors();
      nTupleNo = analysisManager->GetNofNtuples();
      theSD->SetNtupleNum( nTupleNo );
      analysisManager->CreateNtuple( SDnames[i], "Intermediate detector hits");
      analysisManager->CreateNtupleIColumn( nTupleNo, "Nhits" );
      analysisManager->CreateNtupleDColumn( nTupleNo, "x",       *PtrVec[0] );
      analysisManager->CreateNtupleDColumn( nTupleNo, "y",       *PtrVec[1] );
      analysisManager->CreateNtupleDColumn( nTupleNo, "z",       *PtrVec[2] );
      analysisManager->FinishNtuple();
    }
  }
}

/*
 *
 */
void RunAction::EndOfRunAction(const G4Run* aRun){
  fTimer->Stop();
  G4cout << "number of event = " << aRun->GetNumberOfEvent()
         << " " << *fTimer << G4endl;

  // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();
  // save histograms & ntuple
  analysisManager->Write();
  analysisManager->CloseFile();
}
