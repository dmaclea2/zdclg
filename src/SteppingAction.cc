//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SteppingAction.cc
/// \brief Implementation of the SteppingAction class

#include "SteppingAction.hh"

#include "G4Step.hh"
#include "G4Track.hh"
#include "G4OpticalPhoton.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "DetectorConstruction.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"

/*
*/
SteppingAction::SteppingAction()
: G4UserSteppingAction()
{

}

/*
*/
SteppingAction::~SteppingAction()
{

}

/*
*/
void SteppingAction::UserSteppingAction(const G4Step* aStep){

  //(void) step;  
G4Track* theTrack = aStep->GetTrack();
  G4StepPoint* thePostPoint = aStep->GetPostStepPoint();
  G4StepPoint* thePrePoint = aStep->GetPreStepPoint();
  G4VPhysicalVolume* thePostPV = thePostPoint->GetPhysicalVolume();
  G4VPhysicalVolume* thePrePV = thePrePoint->GetPhysicalVolume();
 
  if(thePostPV != NULL){
      if( ( strncmp("Pho",thePrePV->GetName().c_str(),3 ) == 0) ){
          theTrack->SetTrackStatus(fStopAndKill);
      }
  }

}

